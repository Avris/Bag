<?php
namespace Avris\Bag;

class Bag implements \ArrayAccess, \IteratorAggregate, \Countable, \JsonSerializable
{
    /** @var array */
    protected $array = [];

    public function __construct($array = [])
    {
        foreach (BagHelper::toArray($array) as $key => $value) {
            $this->set($key, $value);
        }
    }

    public function all(): array
    {
        return $this->array;
    }

    public function keys(): array
    {
        return array_keys($this->array);
    }

    public function count(): int
    {
        return count($this->array);
    }

    public function isEmpty(): bool
    {
        return empty($this->array);
    }

    public function get($key, $default = null)
    {
        return $this->array[$key] ?? $default;
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function __invoke($key, $default = null)
    {
        return $this->get($key, $default);
    }

    public function getDeep($key, $default = null)
    {
        return Nested::get($this->array, explode('.', $key), $default);
    }

    public function set($key, $value): self
    {
        $this->array[$key] = $value;

        return $this;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->array[] = $value;
        } else {
            $this->set($offset, $value);
        }
    }

    public function has($key): bool
    {
        if (!is_array($key)) {
            return array_key_exists($key, $this->array);
        }

        foreach ($key as $element) {
            if (array_key_exists($element, $this->array)) {
                return true;
            }
        }

        return false;
    }

    public function offsetExists($offset): bool
    {
        return $this->has($offset);
    }

    public function delete($key): self
    {
        unset($this->array[$key]);

        return $this;
    }

    public function offsetUnset($offset)
    {
        $this->delete($offset);
    }

    public function clear(): self
    {
        $this->array = [];

        return $this;
    }

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->array);
    }

    public function add($array): self
    {
        foreach (BagHelper::toArray($array) as $key => $value) {
            if (!isset($this->array[$key])) {
                $this->set($key, $value);
            }
        }

        return $this;
    }

    public function replace($array): self
    {
        foreach (BagHelper::toArray($array) as $key => $value) {
            $this->set($key, $value);
        }

        return $this;
    }

    public function appendToElement($key, $value): self
    {
        $array = $this->getArrayFromElement($key);
        $array[] = $value;

        return $this->set($key, $array);
    }

    public function prependToElement($key, $value): self
    {
        $array = $this->getArrayFromElement($key);
        array_unshift($array, $value);

        return $this->set($key, $array);
    }

    protected function getArrayFromElement($key)
    {
        $array = $this->get($key, []);

        if (!is_array($array)) {
            throw new \InvalidArgumentException(sprintf(
                'Cannot append to element "%s", because it\'s not an array',
                $key
            ));
        }

        return $array;
    }

    public function map(callable $callback): self
    {
        $result = [];
        foreach ($this->array as $key => $value) {
            $mapped = $callback($key, $value);
            $result[$key] = $mapped;
        }

        return new static($result);
    }

    public function filter(callable $callback): self
    {
        $result = [];
        foreach ($this->array as $key => $value) {
            if ($callback($key, $value)) {
                $result[$key] = $value;
            }
        }

        return new static($result);
    }

    public function flatten(): self
    {
        return new static(iterator_to_array($this->doFlatten($this->array)));
    }

    private function doFlatten($array, $base = [])
    {
        foreach ($array as $key => $value) {
            $basedKey = $base;
            $basedKey[] = $key;
            if (is_iterable($value)) {
                yield from $this->doFlatten($value, $basedKey);
            } else {
                yield join('.', $basedKey) => $value;
            }
        }
    }

    public function jsonSerialize(): array
    {
        return $this->array;
    }

    public function __debugInfo(): array
    {
        return $this->array;
    }
}
