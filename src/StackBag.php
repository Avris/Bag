<?php
namespace Avris\Bag;

class StackBag extends Bag
{
    public function push($key, $value)
    {
        $this->set($key, $value);
    }

    public function pop()
    {
        if ($this->isEmpty()) {
            throw new \RuntimeException('Cannot pop an empty stack');
        }

        $value = end($this->array);
        $key = key($this->array);
        unset($this->array[$key]);

        return [$key, $value];
    }
}
