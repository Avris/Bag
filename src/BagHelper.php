<?php
namespace Avris\Bag;

final class BagHelper
{
    const THROW_EXCEPTION = '___throw_exception___';

    public static function isArray($object): bool
    {
        return is_array($object) || $object instanceof \Traversable;
    }

    public static function toArray($object): array
    {
        if ($object instanceof Bag) {
            return $object->all();
        }

        if ($object instanceof \Traversable) {
            return iterator_to_array($object);
        }

        if ($object === null) {
            return [];
        }

        return is_array($object) ? $object : [$object];
    }

    public static function magicGetter($object, string $attr, $default = null)
    {
        if ((is_array($object) || $object instanceof \ArrayAccess) && isset($object[$attr])) {
            return $object[$attr];
        } elseif (is_object($object)) {
            if (method_exists($object, $attr)) {
                return call_user_func([$object, $attr]);
            } elseif (method_exists($object, 'get' . ucfirst($attr))) {
                return call_user_func([$object, 'get' . ucfirst($attr)]);
            } elseif (property_exists($object, $attr)) {
                return $object->{$attr};
            } elseif (method_exists($object, 'get')) {
                return $object->get($attr);
            }
        }

        if ($default === self::THROW_EXCEPTION) {
            throw new NotFoundException;
        }

        return $default;
    }

    public static function magicSetter(&$object, string $attr, $value)
    {
        if (!is_array($object) && !is_object($object)) {
            throw new \InvalidArgumentException(sprintf('Array or object expected, %s given', gettype($object)));
        }

        if (is_array($object) || $object instanceof \ArrayAccess) {
            $object[$attr] = $value;
        } elseif (method_exists($object, 'set' . ucfirst($attr))) {
            call_user_func([$object, 'set' . ucfirst($attr)], $value);
        } elseif (property_exists($object, $attr)) {
            $object->{$attr} = $value;
        } elseif (method_exists($object, 'set')) {
            $object->set($attr, $value);
        } else {
            $object->{$attr} = $value;
        }

        return $object;
    }
}
