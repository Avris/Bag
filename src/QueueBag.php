<?php
namespace Avris\Bag;

class QueueBag extends Bag
{
    public function enqueue($key, $value)
    {
        $this->set($key, $value);
    }

    public function dequeue()
    {
        if ($this->isEmpty()) {
            throw new \RuntimeException('Cannot dequeue an empty queue');
        }

        $value = reset($this->array);
        $key = key($this->array);
        unset($this->array[$key]);

        return [$key, $value];
    }
}
