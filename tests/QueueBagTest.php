<?php
namespace Avris\Bag;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Bag\QueueBag
 */
final class QueueBagTest extends TestCase
{
    public function testQueue()
    {
        $queue = new QueueBag([
            'foo' => 'FOO',
        ]);

        $queue->enqueue('bar', 'BAR');
        $queue->enqueue('baz', 'BAZ');

        list($key, $value) = $queue->dequeue();
        $this->assertEquals('foo', $key);
        $this->assertEquals('FOO', $value);

        list($key, $value) = $queue->dequeue();
        $this->assertEquals('bar', $key);
        $this->assertEquals('BAR', $value);

        list($key, $value) = $queue->dequeue();
        $this->assertEquals('baz', $key);
        $this->assertEquals('BAZ', $value);

        try {
            $queue->dequeue();
            $this->fail('Exception expected');
        } catch (\RuntimeException $e) {
            $this->assertEquals('Cannot dequeue an empty queue', $e->getMessage());
        }
    }

    public function testFilter()
    {
        $queue = new QueueBag([
            'foo' => 'FOO',
            'osiem' => 'OSIEM',
            'foobar' => 'FOOBAR',
        ]);

        $expected = new QueueBag([
            'foo' => 'FOO',
            'foobar' => 'FOOBAR',
        ]);

        $this->assertEquals($expected, $queue->filter(function ($key) { return substr($key, 0, 3) === 'foo'; }));
    }
}