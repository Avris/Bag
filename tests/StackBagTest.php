<?php
namespace Avris\Bag;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Bag\StackBag
 */
final class StackBagTest extends TestCase
{
    public function testQueue()
    {
        $queue = new StackBag([
            'foo' => 'FOO',
        ]);

        $queue->push('bar', 'BAR');
        $queue->push('baz', 'BAZ');

        list($key, $value) = $queue->pop();
        $this->assertEquals('baz', $key);
        $this->assertEquals('BAZ', $value);

        list($key, $value) = $queue->pop();
        $this->assertEquals('bar', $key);
        $this->assertEquals('BAR', $value);

        list($key, $value) = $queue->pop();
        $this->assertEquals('foo', $key);
        $this->assertEquals('FOO', $value);

        try {
            $queue->pop();
            $this->fail('Exception expected');
        } catch (\RuntimeException $e) {
            $this->assertEquals('Cannot pop an empty stack', $e->getMessage());
        }
    }

    public function testMap()
    {
        $stack = new StackBag([
            'foo' => 'FOO',
            'osiem' => 'OSIEM',
            'foobar' => 'FOOBAR',
        ]);

        $expected = new StackBag([
            'foo' => 'FOO8',
            'osiem' => 'OSIEM8',
            'foobar' => 'FOOBAR8',
        ]);

        $this->assertEquals($expected, $stack->map(function ($key, $value) { return $value . '8'; }));
    }
}
