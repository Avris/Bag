<?php
namespace Avris\Bag;

class Set implements \IteratorAggregate, \Countable, \JsonSerializable
{
    /** @var array */
    protected $values = [];

    /** @var callback|null */
    protected $callback;

    public function __construct($values = [], callable $callback = null)
    {
        $values = $values instanceof self ? $values->all() : BagHelper::toArray($values);

        $this->callback = $callback;
        if ($callback) {
            $values = array_map($callback, $values);
        }

        $this->values = array_unique($values);
    }

    public function all()
    {
        return array_values($this->values);
    }

    public function count(): int
    {
        return count($this->values);
    }

    public function isEmpty(): bool
    {
        return count($this->values) === 0;
    }

    protected function normalizeValue($value)
    {
        $callback = $this->callback;

        return $callback ? $callback($value) : $value;
    }

    public function add($value): self
    {
        $value = $this->normalizeValue($value);

        if (!in_array($value, $this->values, true)) {
            $this->values[] = $value;
        }

        return $this;
    }

    public function addMultiple(array $values): self
    {
        foreach ($values as $value) {
            $this->add($value);
        }

        return $this;
    }

    public function has($value): bool
    {
        return in_array($this->normalizeValue($value), $this->values, true);
    }

    public function delete($value): self
    {
        $key = array_search($this->normalizeValue($value), $this->values, true);

        if ($key !== false) {
            unset($this->values[$key]);
        }

        return $this;
    }

    public function first()
    {
        return reset($this->values);
    }

    public function last()
    {
        return end($this->values);
    }

    public function clear(): self
    {
        $this->values = [];

        return $this;
    }

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->values);
    }

    public function jsonSerialize(): array
    {
        return array_values($this->values);
    }

    public function __debugInfo(): array
    {
        return array_values($this->values);
    }
}
